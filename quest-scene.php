<div data-role="page" id="quest-scene-page" data-theme="j">
	<header>
		<div class="user-status">
			<div class="level"><div class="label">Lv.</div><div class="value">12</div></div>
			<div class="exp"><div class="bar-holder"><div class="bar" style='width:75%'></div><div class="value">100/133</div></div></div>
			<div class="energy"><div class="bar-holder"><div class="bar" style='width:100%'></div><div class="value">250/250</div></div></div>
			<div class="spirits"><div class="bar-holder"><div class="bar" style='width:66%'></div><div class="value">100/150</div></div></div>
		</div>
	</header>
	<div class="screen">
	</div>
	<div class="button-holder"><a class="text-button mini quest-button">探索</a></div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
	<div class="assets">
		<div class="asset" data-role="normal-effect" data-name="treasure-box" data-callback-count="1" data-callback-event="webkitAnimationEnd"></div>
	</div>
	<div data-role="popup" id="quest-treasure-popup" data-transition="pop" data-position-to="#quest-scene-page .screen">
		<div class="treasure-content">
			<div class="monster-status">
				<div class="monster-name">モンスター名</div>
				<div class="monster-rarity">★★★</div>
			</div>
			<div class="monster-image"><img src="./img/monsters/pipo-enemy007.png" alt="" width="120" height="120" /></div>
		</div>
	</div>
</div>