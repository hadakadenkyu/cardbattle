<div data-role="page" id="quest-area-list-page" data-theme="j">
	<header>
		<h1>探索 &gt; 地域一覧 &gt; エリア一覧</h1>
	</header>
	<div class="content">
	<ol class="box-list-holder">
		<li class="area-7">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=7">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア7</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-6">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=6">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア6</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-5">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=5">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア5</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-4">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=4">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア4</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-3">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=3">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア3</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-2">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=2">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア2</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
		<li class="area-1">
			<a href="" data-href-id="#quest-block-list-page" data-param="area=1">
				<div class="area-thumb"></div>
				<div class="area-status">
				<div class="area-name">エリア1</div>
				<div class="area-progress">
					<div class="bar-holder">
						<div class="bar" style="width:50%"></div>
						<div class="value">50%</div>
					</div>
				</div>
				</div>
			</a>
		</li>
	</ol>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</div>