<section data-role="page" id="home-page" data-theme="j">
	<header>
		<div class="user-status">
			<div class="level"><div class="label">Lv.</div><div class="value">12</div></div>
			<div class="exp"><div class="bar-holder"><div class="bar" style='width:75%'></div><div class="value">100/133</div></div></div>
			<div class="energy"><div class="bar-holder"><div class="bar" style='width:100%'></div><div class="value">250/250</div></div></div>
			<div class="spirits"><div class="bar-holder"><div class="bar" style='width:66%'></div><div class="value">100/150</div></div></div>
		</div>
	</header>
	<div class="user-deck">
		<ul>
			<li class="monster-6"><div style='background-image:url("./img/monsters/pipo-enemy018.png");'></div></li>
			<li class="monster-5"><div style='background-image:url("./img/monsters/pipo-enemy016.png");'></div></li>
			<li class="monster-4"><div style='background-image:url("./img/monsters/pipo-enemy020.png");'></div></li>
			<li class="monster-3"><div style='background-image:url("./img/monsters/pipo-enemy021.png");'></div></li>
			<li class="monster-2"><div style='background-image:url("./img/monsters/pipo-enemy022.png");'></div></li>
			<li class="monster-1"><div style='background-image:url("./img/monsters/pipo-enemy023.png");'></div></li>
		</ul>
	</div>
	<div class="main-menu">
		<ul>
			<li><a href="#quest-region-list-page" class="text-button mini quest-button">探索</a></li>
			<li><a href="#fusion-list-page" class="text-button mini fusion-button">合成</a></li>
			<li><a href="#gacha-list-page" class="text-button mini gacha-button">ガチャ</a></li>
			<li><a href="#battle-list-page" class="text-button mini battle-button">バトル</a></li>
		</ul>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</section>