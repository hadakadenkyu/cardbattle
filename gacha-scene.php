<div data-role="page" id="gacha-scene-page" data-theme="j">
	<div class="screen">
	</div>
	<div class="button-holder"><a href="#gacha-list-page" class="text-button mini">次へ</a></div>
	<!-- <footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer> -->
</div>
<div class="assets">
	<div data-name="gacha-machine" class="asset">
		<img src="./img/gacha/other_gacha01.png" alt="" width="300" height="300" />
	</div>
	<div data-role="normal-effect" data-name="gacha-capsule" class="asset normal-effect" data-callback-count="1" data-callback-event="webkitAnimationEnd">
		<img src="./img/gacha/other_gacha02_05.png" alt="" width="150" height="150" />
	</div>
	<div data-role="normal-effect" data-name="gacha-content" class="asset normal-effect" data-callback-count="1" data-callback-event="webkitTransitionEnd">
		<img src="./img/monsters/pipo-enemy007.png" alt="" width="120" height="120" />
	</div>
	<div data-role="normal-effect" data-name="gacha-status-info" class="asset normal-effect" data-callback-count="1" data-callback-event="webkitTransitionEnd">
		<div class="monster-status">
			<div class="monster-name">モンスター名</div>
			<div class="monster-rarity">★★★</div>
		</div>
	</div>
</div>